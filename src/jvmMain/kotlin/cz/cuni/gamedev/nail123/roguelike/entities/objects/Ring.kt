package cz.cuni.gamedev.nail123.roguelike.entities.objects

import cz.cuni.gamedev.nail123.roguelike.entities.GameEntity
import cz.cuni.gamedev.nail123.roguelike.entities.Player
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.*
import cz.cuni.gamedev.nail123.roguelike.events.GameOver
import cz.cuni.gamedev.nail123.roguelike.events.GameWin
import cz.cuni.gamedev.nail123.roguelike.mechanics.Combat
import cz.cuni.gamedev.nail123.roguelike.mechanics.Magic
import cz.cuni.gamedev.nail123.roguelike.tiles.GameTiles
import org.hexworks.cobalt.databinding.api.extension.createPropertyFrom

class Ring: GameEntity(GameTiles.RING), Interactable
{
    override val blocksMovement: Boolean
        get() = true
    override val blocksVision: Boolean
        get() = false

    override fun acceptInteractFrom(other: GameEntity, type: InteractionType) = interactionContext(other, type)
    {
        withEntity<Player>(InteractionType.BUMPED)
        {
            GameWin(this).emit()
        }
    }
}