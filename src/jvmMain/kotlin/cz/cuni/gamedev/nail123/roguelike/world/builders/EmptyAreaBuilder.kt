package cz.cuni.gamedev.nail123.roguelike.world.builders

import cz.cuni.gamedev.nail123.roguelike.GameConfig
import cz.cuni.gamedev.nail123.roguelike.blocks.Floor
import cz.cuni.gamedev.nail123.roguelike.blocks.Wall
import cz.cuni.gamedev.nail123.roguelike.entities.enemies.Rat
import cz.cuni.gamedev.nail123.roguelike.mechanics.Pathfinding
import org.hexworks.zircon.api.data.Position3D
import org.hexworks.zircon.api.data.Size3D

/**
 * A builder that creates an empty area with a border.
 */
class EmptyAreaBuilder(size: Size3D = GameConfig.AREA_SIZE,
                       visibleSize: Size3D = GameConfig.VISIBLE_SIZE): AreaBuilder(size, visibleSize)
{

    val xCoordList = mutableListOf(1)
    val yCoordList = mutableListOf(1)

    val xStartCoordList = mutableListOf(1)
    val yStartCoordList = mutableListOf(1)
    val startDirList = mutableListOf(1)

    override fun create(): AreaBuilder
    {
        for (x in 0 until width)
        {
            for (y in 0 until height)
            {
                val isBorder = x == 0 || x == width - 1 || y == 0 || y == height - 1
                blocks[Position3D.create(x, y, 0)] = if (isBorder) Wall() else Floor()
            }
        }

        var numberOfCorridors = (1..2).random()

        for (z in 0 until 15)
        {
            CreateCorridor()
        }

        for (j in 0 until startDirList.size)
        {
            if (j > 0)
            {
                CreateRoom(6, 6, xStartCoordList[j], yStartCoordList[j], startDirList[j])
            }
        }

        return this
    }

    fun Up (y: Int): Int
    {
        var newY = 0

        newY = y + 1
        return newY
    }

    fun Down (y: Int): Int
    {
        var newY = 0

        newY = y - 1
        return newY
    }

    fun Left (x: Int): Int
    {
        var newX = 0

        newX = x - 1
        return  newX
    }

    fun Right (x: Int): Int
    {
        var newX = 0

        newX  = x + 1
        return newX
    }

    fun HorizontalCorridor(X: Int, Y: Int)
    {
        blocks[Position3D.create(X, Y - 2, 0)] = Wall()
        blocks[Position3D.create(X, Y + 2, 0)] = Wall()

        xCoordList.add(X)
        yCoordList.add(Y - 2)

        xCoordList.add(X)
        yCoordList.add(Y + 2)

        blocks[Position3D.create(X, Y - 1, 0)] = Floor()
        blocks[Position3D.create(X, Y, 0)] = Floor()
        blocks[Position3D.create(X, Y + 1, 0)] = Floor()
    }

    fun VerticalCorridor(X: Int, Y: Int)
    {
        blocks[Position3D.create(X - 2, Y, 0)] = Wall()
        blocks[Position3D.create(X + 2, Y, 0)] = Wall()

        xCoordList.add(X - 2)
        yCoordList.add(Y)

        xCoordList.add(X + 2)
        yCoordList.add(Y)

        blocks[Position3D.create(X - 1, Y, 0)] = Floor()
        blocks[Position3D.create(X, Y, 0)] = Floor()
        blocks[Position3D.create(X + 1, Y, 0)] = Floor()
    }

    fun SingleCorridor(X: Int, Y: Int)
    {
        blocks[Position3D.create(X, Y, 0)] = Wall()
    }

    fun CompletionTurnFromTopToLeft(X: Int, Y: Int)
    {
        blocks[Position3D.create(X + 2, Y + 1, 0)] = Wall()
        blocks[Position3D.create(X + 2, Y + 2, 0)] = Wall()
        blocks[Position3D.create(X + 2, Y + 3, 0)] = Wall()
        blocks[Position3D.create(X + 2, Y + 4, 0)] = Wall()
        blocks[Position3D.create(X + 1, Y + 4, 0)] = Wall()
        blocks[Position3D.create(X, Y + 4, 0)] = Wall()
        blocks[Position3D.create(X - 1, Y + 4, 0)] = Wall()
    }

    fun CompletionTurnFromTopToRight(X: Int, Y: Int)
    {
        blocks[Position3D.create(X - 2, Y + 1, 0)] = Wall()
        blocks[Position3D.create(X - 2, Y + 2, 0)] = Wall()
        blocks[Position3D.create(X - 2, Y + 3, 0)] = Wall()
        blocks[Position3D.create(X - 2, Y + 4, 0)] = Wall()
        blocks[Position3D.create(X - 1, Y + 4, 0)] = Wall()
        blocks[Position3D.create(X, Y + 4, 0)] = Wall()
        blocks[Position3D.create(X + 1, Y + 4, 0)] = Wall()
    }

    fun CompletionTurnFromLeftToTop(X: Int, Y: Int)
    {
        blocks[Position3D.create(X - 1, Y - 2, 0)] = Wall()
        blocks[Position3D.create(X - 2, Y - 2, 0)] = Wall()
        blocks[Position3D.create(X - 3, Y - 2, 0)] = Wall()
        blocks[Position3D.create(X - 4, Y - 2, 0)] = Wall()
        blocks[Position3D.create(X - 4, Y - 1, 0)] = Wall()
        blocks[Position3D.create(X - 4, Y, 0)] = Wall()
        blocks[Position3D.create(X - 4, Y + 1, 0)] = Wall()
    }

    fun CompletionTurnFromRightToTop(X: Int, Y: Int)
    {
        blocks[Position3D.create(X + 1, Y - 2, 0)] = Wall()
        blocks[Position3D.create(X + 2, Y - 2, 0)] = Wall()
        blocks[Position3D.create(X + 3, Y - 2, 0)] = Wall()
        blocks[Position3D.create(X + 4, Y - 2, 0)] = Wall()
        blocks[Position3D.create(X + 4, Y - 1, 0)] = Wall()
        blocks[Position3D.create(X + 4, Y, 0)] = Wall()
        blocks[Position3D.create(X + 4, Y + 1, 0)] = Wall()
    }

    fun CompletionTurnFromLeftToDown(X: Int, Y: Int)
    {
        blocks[Position3D.create(X - 1, Y + 2, 0)] = Wall()
        blocks[Position3D.create(X - 2, Y + 2, 0)] = Wall()
        blocks[Position3D.create(X - 3, Y + 2, 0)] = Wall()
        blocks[Position3D.create(X - 4, Y + 2, 0)] = Wall()
        blocks[Position3D.create(X - 4, Y + 1, 0)] = Wall()
        blocks[Position3D.create(X - 4, Y, 0)] = Wall()
        blocks[Position3D.create(X - 4, Y - 1, 0)] = Wall()
    }

    fun CompletionTurnFromRightToDown(X: Int, Y: Int)
    {
        blocks[Position3D.create(X + 1, Y + 2, 0)] = Wall()
        blocks[Position3D.create(X + 2, Y + 2, 0)] = Wall()
        blocks[Position3D.create(X + 3, Y + 2, 0)] = Wall()
        blocks[Position3D.create(X + 4, Y + 2, 0)] = Wall()
        blocks[Position3D.create(X + 4, Y + 1, 0)] = Wall()
        blocks[Position3D.create(X + 4, Y, 0)] = Wall()
        blocks[Position3D.create(X + 4, Y - 1, 0)] = Wall()
    }

    fun CompletionTurnFromDownToLeft(X: Int, Y: Int)
    {
        blocks[Position3D.create(X + 2, Y - 1, 0)] = Wall()
        blocks[Position3D.create(X + 2, Y - 2, 0)] = Wall()
        blocks[Position3D.create(X + 2, Y - 3, 0)] = Wall()
        blocks[Position3D.create(X + 2, Y - 4, 0)] = Wall()
        blocks[Position3D.create(X + 1, Y - 4, 0)] = Wall()
        blocks[Position3D.create(X, Y - 4, 0)] = Wall()
        blocks[Position3D.create(X - 1, Y - 4, 0)] = Wall()
    }

    fun CompletionTurnFromDownToRight(X: Int, Y: Int)
    {
        blocks[Position3D.create(X - 2, Y - 1, 0)] = Wall()
        blocks[Position3D.create(X - 2, Y - 2, 0)] = Wall()
        blocks[Position3D.create(X - 2, Y - 3, 0)] = Wall()
        blocks[Position3D.create(X - 2, Y - 4, 0)] = Wall()
        blocks[Position3D.create(X - 1, Y - 4, 0)] = Wall()
        blocks[Position3D.create(X, Y - 4, 0)] = Wall()
        blocks[Position3D.create(X + 1, Y - 4, 0)] = Wall()
    }

    fun GenerateRandomCoordinationX(): Int
    {
        var X = 0
        X = (1..width - 1).random()
        return X
    }

    fun GenerateRandomCoordinationY(): Int
    {
        var Y = 0
        Y = (1..width - 1).random()
        return Y
    }

    fun BorderCheck(X: Int, Y: Int): Boolean
    {
        var isBorder = false

        if (X <= 0)
        {
            isBorder = true
        }
        else if (X >= width - 1)
        {
            isBorder = true
        }
        else if (Y <= 0)
        {
            isBorder = true
        }
        else if (Y >= height - 1)
        {
            isBorder = true
        }

        return isBorder
    }

    fun WallCheck(X: Int, Y: Int): Boolean
    {
        var isWall = false

        for (i in 0 until xCoordList.size)
        {
            if ((xCoordList[i] == X) and (yCoordList[i] == Y))
            {
                isWall = true
            }
        }

        return isWall
    }

    fun CreateRoom(roomWidth: Int, roomHeight: Int, X: Int, Y: Int, direction: Int)
    {
        //blocks[Position3D.create(X-3, Y + 1, 0)] = Wall()
        //blocks[Position3D.create(X + 3, Y + 1, 0)] = Wall()
        blocks[Position3D.create(X-3, Y, 0)] = Wall()
        blocks[Position3D.create(X + 3, Y, 0)] = Wall()
        blocks[Position3D.create(X - 3, Y - 1, 0)] = Wall()
        blocks[Position3D.create(X - 3, Y - 2, 0)] = Wall()
        //blocks[Position3D.create(X - 3, Y - 3, 0)] = Wall()
        //blocks[Position3D.create(X - 3, Y - 4, 0)] = Wall()
        blocks[Position3D.create(X - 3, Y - 5, 0)] = Wall()
        blocks[Position3D.create(X + 3, Y - 1, 0)] = Wall()
        blocks[Position3D.create(X + 3, Y - 2, 0)] = Wall()
        //blocks[Position3D.create(X + 3, Y - 3, 0)] = Wall()
        //blocks[Position3D.create(X + 3, Y - 4, 0)] = Wall()
        blocks[Position3D.create(X + 3, Y - 5, 0)] = Wall()
        blocks[Position3D.create(X - 2, Y - 5, 0)] = Wall()
        //blocks[Position3D.create(X - 1, Y - 5, 0)] = Wall()
        //blocks[Position3D.create(X, Y - 5, 0)] = Wall()
        //blocks[Position3D.create(X + 1, Y - 5, 0)] = Wall()
        blocks[Position3D.create(X + 2, Y - 5, 0)] = Wall()

        // Cross
        /*blocks[Position3D.create(X, Y, 0)] = Wall()
        blocks[Position3D.create(X + 1, Y, 0)] = Wall()
        blocks[Position3D.create(X - 1, Y, 0)] = Wall()
        blocks[Position3D.create(X, Y + 1, 0)] = Wall()
        blocks[Position3D.create(X, Y - 1, 0)] = Wall()*/
    }

    fun CreateCorridor(): AreaBuilder
    {
        var corridorLength = (20..30).random()

        var coordinateX = (4..width - 4).random()
        var coordinateY = (4..height - 4).random()

        var stop = false
        var wall = false

        var oldDir = 0

        for (i in 0 until corridorLength)
        {
            var direction = (1..4).random()
            var lengthToTurn = (10..20).random()

            if (stop)
            {
                print("STOP")
                return this
            }

            if (wall)
            {
                print("WALL")
                return this
            }

            val startX = coordinateX
            val startY = coordinateY
            val startDir = direction

            xStartCoordList.add(startX)
            yStartCoordList.add(startY)
            startDirList.add(startDir)

            for(j in 0 until lengthToTurn)
            {
                if ((direction == 1) and ((oldDir == 1) or (oldDir == 2) or (oldDir == 0)))
                {
                    // Up
                    oldDir = 1
                    coordinateY = Up(coordinateY)

                    stop = BorderCheck(coordinateX, coordinateY)
                    if (stop)
                    {
                        print("BREAK")
                        break
                    }

                    wall = WallCheck(coordinateX, coordinateY)
                    if (wall)
                    {
                        blocks[Position3D.create(coordinateX, coordinateY, 0)] = Floor()
                        blocks[Position3D.create(coordinateX + 1, coordinateY, 0)] = Floor()
                        blocks[Position3D.create(coordinateX - 1, coordinateY, 0)] = Floor()
                        print("WALL_BREAK")
                        break
                    }

                    VerticalCorridor(coordinateX, coordinateY)
                    //SingleCorridor(coordinateX, coordinateY)
                }
                else if ((direction == 1) and (oldDir == 3))
                {
                    // Up (old = left)
                    oldDir = 1

                    coordinateX = coordinateX - 2;
                    coordinateY = coordinateY + 2;
                    stop = BorderCheck(coordinateX, coordinateY)
                    if (stop)
                    {
                        print("BREAK")
                        break
                    }
                    wall = WallCheck(coordinateX, coordinateY)
                    if (wall)
                    {
                        blocks[Position3D.create(coordinateX, coordinateY, 0)] = Floor()
                        blocks[Position3D.create(coordinateX + 1, coordinateY, 0)] = Floor()
                        blocks[Position3D.create(coordinateX - 1, coordinateY, 0)] = Floor()
                        print("WALL_BREAK")
                        break
                    }

                    coordinateX = coordinateX + 2;
                    coordinateY = coordinateY - 2;
                    CompletionTurnFromLeftToTop(coordinateX, coordinateY)

                    coordinateX = coordinateX - 2;
                    coordinateY = coordinateY + 2;
                    VerticalCorridor(coordinateX, coordinateY)
                }
                else if ((direction == 1) and (oldDir == 4))
                {
                    // Up (old = right)
                    oldDir = 1

                    coordinateX = coordinateX + 2;
                    coordinateY = coordinateY + 2;
                    stop = BorderCheck(coordinateX, coordinateY)
                    if (stop)
                    {
                        print("BREAK")
                        break
                    }
                    wall = WallCheck(coordinateX, coordinateY)
                    if (wall)
                    {
                        blocks[Position3D.create(coordinateX, coordinateY, 0)] = Floor()
                        blocks[Position3D.create(coordinateX + 1, coordinateY, 0)] = Floor()
                        blocks[Position3D.create(coordinateX - 1, coordinateY, 0)] = Floor()
                        print("WALL_BREAK")
                        break
                    }

                    coordinateX = coordinateX - 2;
                    coordinateY = coordinateY - 2;
                    CompletionTurnFromRightToTop(coordinateX, coordinateY)

                    coordinateX = coordinateX + 2;
                    coordinateY = coordinateY + 2;
                    VerticalCorridor(coordinateX, coordinateY)
                }
                else if ((direction == 2) and ((oldDir == 1) or (oldDir == 2) or (oldDir == 0)))
                {
                    // Down
                    oldDir = 2
                    coordinateY = Down(coordinateY)

                    stop = BorderCheck(coordinateX, coordinateY)
                    if (stop)
                    {
                        print("BREAK")
                        break
                    }
                    wall = WallCheck(coordinateX, coordinateY)
                    if (wall)
                    {
                        blocks[Position3D.create(coordinateX, coordinateY, 0)] = Floor()
                        blocks[Position3D.create(coordinateX + 1, coordinateY, 0)] = Floor()
                        blocks[Position3D.create(coordinateX - 1, coordinateY, 0)] = Floor()
                        print("WALL_BREAK")
                        break
                    }

                    VerticalCorridor(coordinateX, coordinateY)
                    //SingleCorridor(coordinateX, coordinateY)
                }
                else if ((direction == 2) and (oldDir == 3))
                {
                    // Down (old = left)
                    oldDir = 2

                    coordinateX = coordinateX - 2;
                    coordinateY = coordinateY - 2;
                    stop = BorderCheck(coordinateX, coordinateY)
                    if (stop)
                    {
                        print("BREAK")
                        break
                    }
                    wall = WallCheck(coordinateX, coordinateY)
                    if (wall)
                    {
                        blocks[Position3D.create(coordinateX, coordinateY, 0)] = Floor()
                        blocks[Position3D.create(coordinateX + 1, coordinateY, 0)] = Floor()
                        blocks[Position3D.create(coordinateX - 1, coordinateY, 0)] = Floor()
                        print("WALL_BREAK")
                        break
                    }

                    coordinateX = coordinateX + 2;
                    coordinateY = coordinateY + 2;
                    CompletionTurnFromLeftToDown(coordinateX, coordinateY)

                    coordinateX = coordinateX - 2;
                    coordinateY = coordinateY - 2;
                    VerticalCorridor(coordinateX, coordinateY)
                }
                else if ((direction == 2) and (oldDir == 4))
                {
                    // Down (old = right)
                    oldDir = 2

                    coordinateX = coordinateX + 2;
                    coordinateY = coordinateY - 2;
                    stop = BorderCheck(coordinateX, coordinateY)
                    if (stop)
                    {
                        print("BREAK")
                        break
                    }
                    wall = WallCheck(coordinateX, coordinateY)
                    if (wall)
                    {
                        blocks[Position3D.create(coordinateX, coordinateY, 0)] = Floor()
                        blocks[Position3D.create(coordinateX + 1, coordinateY, 0)] = Floor()
                        blocks[Position3D.create(coordinateX - 1, coordinateY, 0)] = Floor()
                        print("WALL_BREAK")
                        break
                    }

                    coordinateX = coordinateX - 2;
                    coordinateY = coordinateY + 2;
                    CompletionTurnFromRightToDown(coordinateX, coordinateY)

                    coordinateX = coordinateX + 2;
                    coordinateY = coordinateY - 2;
                    VerticalCorridor(coordinateX, coordinateY)
                }
                else if ((direction == 3) and ((oldDir == 3) or (oldDir == 4) or (oldDir == 0)))
                {
                    // Left
                    oldDir = 3
                    coordinateX = Left(coordinateX)

                    stop = BorderCheck(coordinateX, coordinateY)
                    if (stop)
                    {
                        print("BREAK")
                        break
                    }
                    wall = WallCheck(coordinateX, coordinateY)
                    if (wall)
                    {
                        blocks[Position3D.create(coordinateX, coordinateY, 0)] = Floor()
                        blocks[Position3D.create(coordinateX, coordinateY + 1, 0)] = Floor()
                        blocks[Position3D.create(coordinateX, coordinateY - 1, 0)] = Floor()
                        print("WALL_BREAK")
                        break
                    }

                    HorizontalCorridor(coordinateX, coordinateY)
                    //SingleCorridor(coordinateX, coordinateY)
                }
                else if ((direction == 3) and (oldDir == 1))
                {
                    // Left (old - up)
                    oldDir = 3

                    coordinateX = coordinateX - 2;
                    coordinateY = coordinateY + 2;
                    stop = BorderCheck(coordinateX, coordinateY)
                    if (stop)
                    {
                        print("BREAK")
                        break
                    }
                    wall = WallCheck(coordinateX, coordinateY)
                    if (wall)
                    {
                        blocks[Position3D.create(coordinateX, coordinateY, 0)] = Floor()
                        blocks[Position3D.create(coordinateX, coordinateY + 1, 0)] = Floor()
                        blocks[Position3D.create(coordinateX, coordinateY - 1, 0)] = Floor()
                        print("WALL_BREAK")
                        break
                    }

                    coordinateX = coordinateX + 2;
                    coordinateY = coordinateY - 2;
                    CompletionTurnFromTopToLeft(coordinateX, coordinateY)

                    coordinateX = coordinateX - 2;
                    coordinateY = coordinateY + 2;
                    HorizontalCorridor(coordinateX, coordinateY)
                }
                else if ((direction == 3) and (oldDir == 2))
                {
                    // Left (old - down)
                    oldDir = 3

                    coordinateX = coordinateX - 2;
                    coordinateY = coordinateY - 2;
                    stop = BorderCheck(coordinateX, coordinateY)
                    if (stop)
                    {
                        print("BREAK")
                        break
                    }
                    wall = WallCheck(coordinateX, coordinateY)
                    if (wall)
                    {
                        blocks[Position3D.create(coordinateX, coordinateY, 0)] = Floor()
                        blocks[Position3D.create(coordinateX, coordinateY + 1, 0)] = Floor()
                        blocks[Position3D.create(coordinateX, coordinateY - 1, 0)] = Floor()
                        print("WALL_BREAK")
                        break
                    }

                    coordinateX = coordinateX + 2;
                    coordinateY = coordinateY + 2;
                    CompletionTurnFromDownToLeft(coordinateX, coordinateY)

                    coordinateX = coordinateX - 2;
                    coordinateY = coordinateY - 2;
                    HorizontalCorridor(coordinateX, coordinateY)
                }
                else if ((direction == 4) and ((oldDir == 3) or (oldDir == 4) or (oldDir == 0)))
                {
                    // Right
                    oldDir = 4
                    coordinateX = Right(coordinateX)

                    stop = BorderCheck(coordinateX, coordinateY)
                    if (stop)
                    {
                        print("BREAK")
                        break
                    }
                    wall = WallCheck(coordinateX, coordinateY)
                    if (wall)
                    {
                        blocks[Position3D.create(coordinateX, coordinateY, 0)] = Floor()
                        blocks[Position3D.create(coordinateX, coordinateY + 1, 0)] = Floor()
                        blocks[Position3D.create(coordinateX, coordinateY - 1, 0)] = Floor()
                        print("WALL_BREAK")
                        break
                    }

                    HorizontalCorridor(coordinateX, coordinateY)
                    //SingleCorridor(coordinateX, coordinateY)
                }
                else if ((direction == 4) and (oldDir == 1))
                {
                    // Right (old - up)
                    oldDir = 4

                    coordinateX = coordinateX + 2;
                    coordinateY = coordinateY + 2;
                    stop = BorderCheck(coordinateX, coordinateY)
                    if (stop)
                    {
                        print("BREAK")
                        break
                    }
                    wall = WallCheck(coordinateX, coordinateY)
                    if (wall)
                    {
                        blocks[Position3D.create(coordinateX, coordinateY, 0)] = Floor()
                        blocks[Position3D.create(coordinateX, coordinateY + 1, 0)] = Floor()
                        blocks[Position3D.create(coordinateX, coordinateY - 1, 0)] = Floor()
                        print("WALL_BREAK")
                        break
                    }

                    coordinateX = coordinateX - 2;
                    coordinateY = coordinateY - 2;
                    CompletionTurnFromTopToRight(coordinateX, coordinateY)

                    coordinateX = coordinateX + 2;
                    coordinateY = coordinateY + 2;
                    HorizontalCorridor(coordinateX, coordinateY)
                }
                else if ((direction == 4) and (oldDir == 2))
                {
                    // Right (old - down)
                    oldDir = 4

                    coordinateX = coordinateX + 2;
                    coordinateY = coordinateY - 2;
                    stop = BorderCheck(coordinateX, coordinateY)
                    if (stop)
                    {
                        print("BREAK")
                        break
                    }
                    wall = WallCheck(coordinateX, coordinateY)
                    if (wall)
                    {
                        blocks[Position3D.create(coordinateX, coordinateY, 0)] = Floor()
                        blocks[Position3D.create(coordinateX, coordinateY + 1, 0)] = Floor()
                        blocks[Position3D.create(coordinateX, coordinateY - 1, 0)] = Floor()
                        print("WALL_BREAK")
                        break
                    }

                    coordinateX = coordinateX - 2;
                    coordinateY = coordinateY + 2;
                    CompletionTurnFromDownToRight(coordinateX, coordinateY)

                    coordinateX = coordinateX + 2;
                    coordinateY = coordinateY - 2;
                    HorizontalCorridor(coordinateX, coordinateY)
                }
            }

        }

        return this
    }
}