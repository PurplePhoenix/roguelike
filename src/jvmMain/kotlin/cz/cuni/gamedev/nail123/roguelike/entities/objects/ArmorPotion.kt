package cz.cuni.gamedev.nail123.roguelike.entities.objects

import cz.cuni.gamedev.nail123.roguelike.entities.GameEntity
import cz.cuni.gamedev.nail123.roguelike.entities.Player
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.*
import cz.cuni.gamedev.nail123.roguelike.mechanics.Combat
import cz.cuni.gamedev.nail123.roguelike.mechanics.Magic
import cz.cuni.gamedev.nail123.roguelike.tiles.GameTiles
import org.hexworks.cobalt.databinding.api.extension.createPropertyFrom

class ArmorPotion: GameEntity(GameTiles.ARMOR), Interactable
{
    override val blocksMovement: Boolean
        get() = true
    override val blocksVision: Boolean
        get() = false

    override fun acceptInteractFrom(other: GameEntity, type: InteractionType) = interactionContext(other, type)
    {
        withEntity<Player>(InteractionType.BUMPED)
        {
            player -> Magic.ArmorStrengthening(player)
            (this@ArmorPotion as GameEntity?)?.area?.removeEntity(this@ArmorPotion)
        }
    }
}