package cz.cuni.gamedev.nail123.roguelike.entities.objects

import cz.cuni.gamedev.nail123.roguelike.entities.GameEntity
import cz.cuni.gamedev.nail123.roguelike.entities.Player
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.*
import cz.cuni.gamedev.nail123.roguelike.mechanics.Combat
import cz.cuni.gamedev.nail123.roguelike.mechanics.Magic
import cz.cuni.gamedev.nail123.roguelike.tiles.GameTiles
import org.hexworks.cobalt.databinding.api.extension.createPropertyFrom

class AttackPotion: GameEntity(GameTiles.ATTACK), Interactable
{
    override val blocksMovement: Boolean
        get() = true
    override val blocksVision: Boolean
        get() = false

    override fun acceptInteractFrom(other: GameEntity, type: InteractionType) = interactionContext(other, type)
    {
        withEntity<Player>(InteractionType.BUMPED)
        {
            player -> Magic.AttackBoost(player)
            (this@AttackPotion as GameEntity?)?.area?.removeEntity(this@AttackPotion)
        }
    }
}