package cz.cuni.gamedev.nail123.roguelike.mechanics

import cz.cuni.gamedev.nail123.roguelike.entities.Player

object Magic
{
    fun Heal (caster: Player)
    {
        caster.hitpoints++
    }

    fun ArmorStrengthening (caster: Player)
    {
        caster.defense++
    }

    fun AttackBoost (caster: Player)
    {
        caster.attack++
    }
}