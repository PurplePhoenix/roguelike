package cz.cuni.gamedev.nail123.roguelike.world.worlds

import cz.cuni.gamedev.nail123.roguelike.blocks.Floor
import cz.cuni.gamedev.nail123.roguelike.blocks.Wall
import cz.cuni.gamedev.nail123.roguelike.entities.enemies.*
import cz.cuni.gamedev.nail123.roguelike.entities.objects.*
import cz.cuni.gamedev.nail123.roguelike.entities.unplacable.FogOfWar
import cz.cuni.gamedev.nail123.roguelike.events.logMessage
import cz.cuni.gamedev.nail123.roguelike.mechanics.Pathfinding
import cz.cuni.gamedev.nail123.roguelike.world.Area
import cz.cuni.gamedev.nail123.roguelike.world.World
import cz.cuni.gamedev.nail123.roguelike.world.builders.AreaBuilder
import cz.cuni.gamedev.nail123.roguelike.world.builders.EmptyAreaBuilder
import org.hexworks.zircon.api.data.Position3D
import org.hexworks.zircon.api.data.Size3D

/**
 * Sample world, made as a starting point for creating custom worlds.
 * It consists of separate levels - each one has one staircase, and it leads infinitely deep.
 */
class SampleWorld: World()
{
    var currentLevel = 0
    val levels
        get() = areas

    override fun buildStartingArea() = buildLevel(0)

    /**
     * Builds one of the levels.
     */
    fun buildLevel(floor: Int): Area
    {
        val areaBuilder = EmptyAreaBuilder().create()

        areaBuilder.addEntity(areaBuilder.player, Position3D.create(1, 1, 0))

        // Add stairs down
        val floodFill = Pathfinding.floodFill(Position3D.create(1, 1, 0), areaBuilder)
        val staircasePosition = PathControl(floodFill)

        if (floodFill.isNotEmpty())
        {
            areaBuilder.blocks[Position3D.create(staircasePosition.x, staircasePosition.y, 0)] = Floor()
            areaBuilder.addEntity(Stairs(), staircasePosition)
        }
        else
        {
            areaBuilder.addAtEmptyPosition(
                    Stairs(),
                    Position3D.create(areaBuilder.width / 2, areaBuilder.height / 2, 0),
                    Size3D.create(areaBuilder.width / 2 - 2, areaBuilder.height / 2 - 2, 1)
            )
        }

        repeat(currentLevel + 1)
        {
            areaBuilder.addAtEmptyPosition(Potion(), Position3D.defaultPosition(), areaBuilder.size)
        }

        repeat(currentLevel + 1)
        {
            areaBuilder.addAtEmptyPosition(ArmorPotion(), Position3D.defaultPosition(), areaBuilder.size)
        }

        repeat(currentLevel + 1)
        {
            areaBuilder.addAtEmptyPosition(AttackPotion(), Position3D.defaultPosition(), areaBuilder.size)
        }

        // Add some rats to each level
        repeat(currentLevel + 1)
        {
            areaBuilder.addAtEmptyPosition(Rat(), Position3D.defaultPosition(), areaBuilder.size)
        }

        if (currentLevel > 1)
        {
            repeat(currentLevel + 1)
            {
                areaBuilder.addAtEmptyPosition(Snake(), Position3D.defaultPosition(), areaBuilder.size)
            }
        }

        if (currentLevel > 2)
        {
            repeat(currentLevel)
            {
                areaBuilder.addAtEmptyPosition(Orc(), Position3D.defaultPosition(), areaBuilder.size)
            }
        }

        if (currentLevel > 4)
        {
            repeat(currentLevel - 1)
            {
                areaBuilder.addAtEmptyPosition(Ghost(), Position3D.defaultPosition(), areaBuilder.size)
            }
        }

        if (currentLevel > 5)
        {
            repeat(currentLevel - 2)
            {
                areaBuilder.addAtEmptyPosition(Golem(), Position3D.defaultPosition(), areaBuilder.size)
            }
        }

        if (currentLevel > 8)
        {
            areaBuilder.addAtEmptyPosition(Ring(), Position3D.defaultPosition(), areaBuilder.size)
        }

        // We add fog of war such that exploration is needed
        //areaBuilder.addEntity(FogOfWar(), Position3D.unknown())

        // Build it into a full Area
        return areaBuilder.build()
    }

    /**
     * Moving down - goes to a brand new level.
     */
    override fun moveDown()
    {
        if (currentLevel == 0)
        {
            this.logMessage("Finally, you have come. The lost and deserted city of dwarves. Somewhere here, on the lower levels, teeming with monsters, one of the Seven Rings of dwarves still passed to them by Sauron is still kept. Can you get to it?")
        }

        if (currentLevel == 1)
        {
            this.logMessage("You go down below. Around the rat.")
        }

        if (currentLevel == 2)
        {
            this.logMessage("You keep going down. Among rats, snakes begin to meet.")
        }

        if (currentLevel == 3)
        {
            this.logMessage("You have overcome the next level. In these underground halls live faithful hearings of sauron - orcs.")
        }

        if (currentLevel == 4)
        {
            this.logMessage("Lower and lower. You feel the presence of ghosts.")
        }

        if (currentLevel == 5)
        {
            this.logMessage("You keep going down. You know that you will soon meet with one of the enemy's most disgusting creatures - golems.")
        }

        if (currentLevel == 6)
        {
            this.logMessage("It seems that there will be no more surprises.")
        }

        if (currentLevel == 7)
        {
            this.logMessage("With each level down, the number of enemies is multiplying. What will await you in the end?")
        }

        if (currentLevel == 8)
        {
            this.logMessage("You are close. You already hear the call of the Ring.")
        }

        if (currentLevel > 8)
        {
            this.logMessage("You feel that the Ring is here. It is calling. It remains only to break through to him through the hordes of monsters.")
        }

        ++currentLevel
        this.logMessage("Descended to level ${currentLevel + 1}")

        if (currentLevel >= areas.size)
        {
            areas.add(buildLevel(levels.size))
        }

        goToArea(areas[currentLevel])
    }

    /**
     * Moving up would be for revisiting past levels, we do not need that. Check [DungeonWorld] for an implementation.
     */
    override fun moveUp()
    {
        // Not implemented
    }

    fun PathControl(floodFill: Map<Position3D, Int>): Position3D
    {
        var staircasePosition = floodFill.keys.last()
        var index = floodFill.keys.count()

        while (index > 0)
        {
            if ((staircasePosition.x + staircasePosition.y > 20))
            {
                return  staircasePosition
            }

            index -= 1
            staircasePosition = floodFill.keys.elementAt(index)

            if (index == 1)
            {
                staircasePosition = floodFill.keys.last()
                return  staircasePosition
            }
        }

        return  staircasePosition
    }

    fun Up (y: Int): Int
    {
        var newY = 0

        newY = y + 1
        return newY
    }

    fun Down (y: Int): Int
    {
        var newY = 0

        newY = y - 1
        return newY
    }

    fun Left (x: Int): Int
    {
        var newX = 0

        newX = x - 1
        return  newX
    }

    fun Right (x: Int): Int
    {
        var newX = 0

        newX  = x + 1
        return newX
    }

    fun HorizontalCorridor(X: Int, Y: Int)
    {
        EmptyAreaBuilder().blocks[Position3D.create(X, Y - 2, 0)] = Wall()
        EmptyAreaBuilder().blocks[Position3D.create(X, Y + 2, 0)] = Wall()
    }

    fun VerticalCorridor(X: Int, Y: Int)
    {
        EmptyAreaBuilder().blocks[Position3D.create(X - 2, Y, 0)] = Wall()
        EmptyAreaBuilder().blocks[Position3D.create(X + 2, Y, 0)] = Wall()
    }

    fun CreateCorridor(areaBuilder: AreaBuilder)
    {
        var numberOfTurns = (0..10).random()
        var lengthToTurn = (0..10).random()
        var randomStartX = (0..10).random()
        var randomStartY = (0..10).random()

        var corridorLength = (10..20).random()
        var coordinateX = (1..areaBuilder.width - 1).random()
        var coordinateY = (1..areaBuilder.height - 1).random()
        var coordinateZ = 0

        for (x in 0 until corridorLength)
        {
            var direction = (1..4).random()

            if (direction == 1)
            {
                // Up
                coordinateY = Up(coordinateY)
                VerticalCorridor(coordinateX, coordinateY)
            }
            else if (direction == 2)
            {
                // Down
                coordinateY = Down(coordinateY)
                VerticalCorridor(coordinateX, coordinateY)
            }
            else if (direction == 3)
            {
                // Left
                coordinateX = Left(coordinateX)
                HorizontalCorridor(coordinateX, coordinateY)
            }
            else if (direction == 4)
            {
                // Right
                coordinateX = Right(coordinateX)
                HorizontalCorridor(coordinateX, coordinateY)
            }
        }
    }
}
