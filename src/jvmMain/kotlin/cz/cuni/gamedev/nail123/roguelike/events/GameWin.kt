package cz.cuni.gamedev.nail123.roguelike.events

class GameWin(override val emitter: Any): GameEvent()