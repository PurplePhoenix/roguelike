import cz.cuni.gamedev.nail123.roguelike.entities.enemies.*
import cz.cuni.gamedev.nail123.roguelike.entities.items.Axe
import cz.cuni.gamedev.nail123.roguelike.entities.items.Bow
import cz.cuni.gamedev.nail123.roguelike.entities.items.Item
import cz.cuni.gamedev.nail123.roguelike.entities.items.Sword
import kotlin.random.Random

object LootSystem
{
    // Store rng for convenience
    val rng = Random.Default

    // Sword with power 2-4
    val basicSword = SingleDrop { Sword(rng.nextInt(3) + 2) }
    // Sword with power 5-6
    val rareSword = SingleDrop { Sword(rng.nextInt(2) + 5) }

    val basicAxe = SingleDrop { Axe (rng.nextInt(4) + 1)}
    val rareAxe = SingleDrop {Axe(rng.nextInt(6) + 2)}

    val basicBow = SingleDrop { Bow (rng.nextInt(2) + 1)}
    val rareBow = SingleDrop {Bow (rng.nextInt(4) + 3)}

    interface ItemDrop
    {
        fun getDrops(): List<Item>
    }
    object NoDrop: ItemDrop
    {
        override fun getDrops() = listOf<Item>()
    }
    class SingleDrop(val instantiator: () -> Item): ItemDrop
    {
        override fun getDrops() = listOf(instantiator())
    }
    class TreasureClass(val numDrops: Int, val possibleDrops: List<Pair<Int, ItemDrop>>): ItemDrop
    {
        val totalProb = possibleDrops.map { it.first }.sum()

        override fun getDrops(): List<Item>
        {
            val drops = ArrayList<Item>()
            repeat(numDrops)
            {
                drops.addAll(pickDrop().getDrops())
            }
            return drops
        }

        private fun pickDrop(): ItemDrop
        {
            var randNumber = Random.Default.nextInt(totalProb)
            for (drop in possibleDrops)
            {
                randNumber -= drop.first
                if (randNumber < 0) return drop.second
            }
            // Never happens, but we need to place something here anyway
            return possibleDrops.last().second
        }
    }

    val basicDrop = TreasureClass(1, listOf(
            2 to NoDrop,
            1 to basicSword,
            1 to basicBow,
            1 to basicAxe
    ))
    val enemyDrops = mapOf(
            Rat::class to basicDrop,
            Orc::class to TreasureClass(1, listOf(
                    6 to basicDrop,
                    1 to rareSword
            )),
            Ghost::class to TreasureClass(1, listOf(
                    2 to basicDrop,
                    2 to rareSword,
                    2 to rareBow,
                    1 to rareAxe
            )),
            Snake::class to basicDrop,
            Golem::class to TreasureClass(1, listOf(
                    1 to basicDrop,
                    2 to rareAxe,
                    3 to rareSword
            ))
    )

    fun onDeath(enemy: Enemy)
    {
        val drops = enemyDrops[enemy::class]?.getDrops() ?: return

        for (item in drops)
        {
            enemy.area[enemy.position]?.entities?.add(item)
        }
    }
}
